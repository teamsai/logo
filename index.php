<?php

chdir('full');

$files = glob('*.png');

foreach ($files as $filename) {
	if (!file_exists('../thumbs/' . $filename)) {
		list($width, $height) = getimagesize($filename);

		$thumb = imagecreatetruecolor(184, 184);
		$full = imagecreatefrompng($filename);

		imagecopyresampled($thumb, $full, 0, 0, 0, 0, 184, 184, $width, $height);
		imagedestroy($full);

		imagepng($thumb, '../thumbs/' . $filename);
		imagedestroy($thumb);

		chmod('../thumbs/' . $filename, 0660);
	}
}

?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>SAI</title>

	<style>
		body {
			margin: 0;
			padding: 10px;
			background-color: #181818;
		}

		img {
			float: left;
			margin: 10px;
		}

		div {
			float: left;
			clear: both;
		}

		div.thumb {
			clear: none;
			margin: 10px;
			width: 184px;
			text-align: center;
			font: bold 12px Arial, sans-serif;
			color: #fff;
		}

		div.thumb > img {
			margin: 0;
		}

		div.thumb > a {
			color: #ffffff;
			text-decoration: none;
		}

		div.thumb > a.sep {
			margin-left: 4px;
			padding-left: 4px;
			border-left: 1px solid #fff;
		}

		div.space {
			margin-top: 80px;
		}

		img.max-64 {
			max-width: 64px;
			max-height: 64px;
		}

		img.max-32 {
			max-width: 32px;
			max-height: 32px;
		}

	</style>
</head>
<body>
<?php

foreach ($files as $filename) {
	echo '<div class="thumb">';

	printf('<img src="thumbs/%1$s" alt="">', $filename);
	printf('<a href="full/%1$s">PNG</a>', $filename);

	$withoutExtension = substr($filename, 0, -4);

	if (file_exists($withoutExtension . '.psd')) {
		printf('<a class="sep" href="full/%1$s.psd">PSD</a>', $withoutExtension);
	}

	if (file_exists($withoutExtension . '.pdf')) {
		printf('<a class="sep" href="full/%1$s.pdf">PDF</a>', $withoutExtension);
	}

	echo '</div>';
}

echo '<div class="space">';

foreach ($files as $filename) {
	printf('<img class="max-64" src="thumbs/%1$s" alt="">', $filename);
}

echo '</div><div class="space">';

foreach ($files as $filename) {
	printf('<img class="max-32" src="thumbs/%1$s" alt="">', $filename);
}

echo '</div>';

chdir('../img');

$files = glob('*.png');

foreach ($files as $filename) {
	printf('<div class="space"><img src="img/%1$s" alt=""></div>', $filename);
}

?>
<div style="float: none"></div>
</body>
